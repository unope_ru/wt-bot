<hr>
<h1>WT-Bot</h1>
<h2>Bot for automatic pumping in the game <a target="_blank" href="https://game.web-tycoon.com/">Web Tycoon</a></h2>
<img src="./img/wt.webp">
<h2><img src="./img/install.webp" height="54" align="middle"> Installation</h2>
<a target="_blank" href="https://cloud.okteto.com/#/deploy?repository=https://gitlab.com/unope_ru/wt-bot"><img src="https://user-images.githubusercontent.com/36935426/159979786-61a598ef-83c8-4c53-9cda-9aea31d61587.png" height="60"></a>

<b>VDS installation:</b>
Run this command out of <b>root</b>:
<code>. <(wget -qO- http://gg.gg/get_wt)</code>
<hr>
<h2><img src="./img/changes.webp" height="54" align="middle"> Changes</h2>
<ul>
	<li>🆕 Latest Telegram Layers with reactions, video stickers and other stuff</li>
	<li>🔓 Security improvements</li>
	<li>🎨 UI/UX improvements</li>
	<li>📼 New core modules</li>
	<li>☁️ Okteto cloud deployment support</li>
	<li>⏱ Quick bug fixes (compared to official FTG)</li>
	<li>▶️ Inline Buttons, available not only for core modules, but for every developer</li>
	<li>🖼 Inline Galleries for every developer</li>
	<li>🔁 Full backward compatibility with older version and with official FTG\GeekTG modules</li>
</ul>
<hr>
<h2 border="none"><img src="./img/requirements.webp" height="54" align="middle"> Requirements</h2>
<ul>
	<li>Python 3.8 or above</li>
	<li>Requirements from <code>requirements.txt</code></li>
	<li>Optional: API ID and Hash from <a href="https://my.telegram.org/apps" color="#2594cb">Telegram</a> or default ones, if your Telegram account is old enough</li>
	<li>Optional: Some basic knowledge of terminal</li>
</ul>
<hr>
<h2 border="none"><img src="./img/support.webp" height="54" align="middle"> <a target="_blank" href="https://t.me/unope_ru">Support</a></h2>
<hr>
<h2><img src="./img/features.webp" height="54" align="middle"> Forked from <a target="_blank" href="https://github.com/voodee/web-tycoon-tools/">voodee/web-tycoon-tools</a>
</h2>
