FROM node:10-slim
RUN apt-get update 
RUN apt-get install gnupg2 wget apt-transport-https ca-certificates fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst ttf-freefont -y
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt-get install ./google-chrome-stable_current_amd64.deb -y
RUN apt-get install wget unzip fontconfig locales gconf-service libasound2 libatk1.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils -y 
RUN rm -rf /var/lib/apt/lists/* 
RUN rm -rf /src/*.deb
RUN npm i puppeteer
RUN groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser 
RUN mkdir -p /home/pptruser/Downloads 
RUN chown -R pptruser:pptruser /home/pptruser 
RUN chown -R pptruser:pptruser /node_modules
WORKDIR /app
ADD package.json package-lock.json ./
RUN npm install

CMD ["node", "index"]